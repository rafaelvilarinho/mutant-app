# Import the official NodeJS image; which sets up node for us in the container
FROM node:carbon

# Setup app working directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install app dependencies
RUN npm install

# Copy sourcecode
COPY . .

# Run a command with arguments
CMD [ "npm", "start" ]