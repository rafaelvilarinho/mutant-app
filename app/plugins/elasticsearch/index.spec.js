require("dotenv").config();

const assert = require("assert");
const functions = require("./index");

describe("Armazenamento de logs no Elasticsearch", () => {
  it("Armazenando visualização de página", async () => {
    const response = await functions.createLog(functions.types.interactions, {
      actions: functions.actions.view_page,
      description: "test-page",
      timestamp: new Date()
    });

    return assert.notEqual(
      response,
      undefined,
      "Ocorreu um erro durante o armazenamento do log de visualização de página."
    );
  });

  it("Armazenando busca de termo", async () => {
    const response = await functions.createLog(functions.types.interactions, {
      actions: functions.actions.search,
      description: "test term",
      timestamp: new Date()
    });

    return assert.notEqual(
      response,
      undefined,
      "Ocorreu um erro durante o armazenamento do log de busca de termo."
    );
  });
});
