const es = require("elasticsearch");

const indices = {
  logs: "mutant_logs"
};

const types = (module.exports.types = {
  interactions: "interactions"
});

module.exports.actions = {
  view_page: "view_page",
  search: "search"
};

const client = new es.Client({
  host: process.env.elasticsearch_host
});

module.exports.checkHealth = async () => {
  try {
    const data = await client.cluster.health();

    return data;
  } catch (error) {
    console.log("Ocorreu um erro em checkHealth", error);
  }
};

module.exports.createLog = async (type, body) => {
  try {
    const checkIndex = await client.indices.exists({ index: indices.logs });

    var data = {};
    if (!checkIndex)
      data = await client.indices.create({ index: indices.logs });

    if (checkIndex || data.acknowledged) {
      const createDocument = await client.index({
        index: indices.logs,
        type: type,
        body: body
      });

      return createDocument;
    }
  } catch (error) {
    console.log("createIndex error", error, type, body);
  }
};

module.exports.getLogs = async () => {
  try {
    const checkIndex = await client.indices.exists({ index: indices.logs });

    if (checkIndex) {
      const data = await client.search({
        index: indices.logs,
        type: types.interactions,
        size: 1000
      });

      return data;
    } else {
      return [];
    }
  } catch (error) {
    console.log("createIndex error", error);
    return [];
  }
};
