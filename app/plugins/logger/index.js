const elasticsearch = require("../elasticsearch");

module.exports.log_viewpage = async (req, res, next) => {
  try {
    const { headers, originalUrl } = req;

    if (headers["sec-fetch-mode"] === "navigate") {
      const data = await elasticsearch.createLog(
        elasticsearch.types.interactions,
        {
          action: elasticsearch.actions.view_page,
          description: originalUrl,
          timestamp: new Date()
        }
      );
    }

    next();
  } catch (error) {
    console.log("log_viewpage error", error);
  }
};

module.exports.log_searchterm = async (req, res, next) => {
  try {
    const { headers, body } = req;

    if (headers["sec-fetch-mode"] === "navigate") {
      const data = await elasticsearch.createLog(
        elasticsearch.types.interactions,
        {
          action: elasticsearch.actions.search,
          description: body.term || "",
          timestamp: new Date()
        }
      );
    }

    next();
  } catch (error) {
    console.log("log_viewpage error", error);
  }
};
