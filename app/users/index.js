const router = require("express").Router();
const logger = require("../plugins/logger");

const controller = require("./controller");

router.get("/", (req, res) => {
  res.status(200).render("users/index");
});

router.get("/info", async (req, res) => {
  const { list } = await controller.users_info();
  res.status(200).render("users/info", { users: list });
});

router.get("/users-website", async (req, res) => {
  const { list } = await controller.users_website();
  res.status(200).render("users/websites", { users: list });
});

router.get("/address-search", async (req, res) => {
  const { filtered_list, term } = await controller.address_search("suite");
  res
    .status(200)
    .render("users/address-search", { users: filtered_list, term });
});

router.post("/address-search", logger.log_searchterm, async (req, res) => {
  const search_term = req.body.term;
  const { filtered_list, term } = await controller.address_search(search_term);
  res
    .status(200)
    .render("users/address-search", { users: filtered_list, term });
});

router.get("/logs", async (req, res) => {
  const { logs, health } = await controller.logs();
  res.status(200).render("users/logs", { logs, health });
});

module.exports = router;
