require("dotenv").config();

const assert = require("assert");
const controller = require("./controller");

describe("Informações de Usuários", () => {
  it("Testando obtenção de lista de usuários", async () => {
    const { list } = await controller.users_info();

    return assert.notEqual(list, undefined, "A lista de usuários é nula.");
  });
});

describe("Websites de Usuários", () => {
  it("Testando obtenção de lista de usuários", async () => {
    const { list } = await controller.users_website();

    return assert.notEqual(list, undefined, "A lista de usuários é nula.");
  });
});

describe("Pesquisa por endereços (campo suite)", () => {
  it("Testando obtenção de lista de usuários que contenham a palavra 'suite' em seu endereço", async () => {
    const { filtered_list } = await controller.address_search("suite");

    return assert.notEqual(
      filtered_list.length,
      0,
      "Nenhum usuário foi encontrado."
    );
  });
});
