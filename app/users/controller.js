const axios = require("axios").default;
const dataUsersUrl = process.env.data_endpoint;

const elasticsearch = require("../plugins/elasticsearch");

const sortArray = array => {
  array.sort((a, b) => {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;

    return 0;
  });

  return array;
};

module.exports.users_info = async () => {
  var list = [];
  try {
    data = await axios.get(dataUsersUrl);
    list = data.data;

    list = sortArray(list);
  } catch (error) {
    console.log("Ocorreu um erro durante a busca de usuários", error);
  }

  return { list };
};

module.exports.users_website = async () => {
  var list = [];
  try {
    data = await axios.get(dataUsersUrl);
    list = data.data;

    list = sortArray(list);
  } catch (error) {
    console.log("Ocorreu um erro durante a busca de usuários", error);
  }

  return { list };
};

module.exports.address_search = async term => {
  var list = [];
  var filtered_list = [];

  try {
    data = await axios.get(dataUsersUrl);
    list = data.data;

    filtered_list = list.filter(item =>
      new RegExp(`${term}`, "i").test(item.address.suite)
    );

    filtered_list = sortArray(filtered_list);
  } catch (error) {
    console.log("Ocorreu um erro durante a busca de usuários", error);
  }

  return { filtered_list, term };
};

module.exports.logs = async () => {
  var logs = {};
  var health = {};
  try {
    health = await elasticsearch.checkHealth();
    logs = await elasticsearch.getLogs();
  } catch (error) {}

  return { logs, health };
};
