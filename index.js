require("dotenv").config();

const express = require("express");
const app = express();

const logger = require("./app/plugins/logger");

app.set("view engine", "ejs");

app.use(express.static(`public`));
app.use(express.urlencoded());
app.use(express.json());

app.use(logger.log_viewpage);

app.use("/", require("./app/users"));

const port = 8080;
app.listen(port, () => {
  console.log(`Server started on port ${port}.`);
});
